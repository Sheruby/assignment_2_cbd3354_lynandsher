# main.tf

# Provider configuration
provider "aws" {
  region = "us-west-2"  # Change to your desired region
}

# Module for EKS cluster
module "eks_cluster" {
  source     = "terraform-aws-modules/eks/aws"
  cluster_name    = "my-eks-cluster"
  cluster_version = "1.21"
  subnets         = ["subnet-12345678", "subnet-87654321"]  # Add your subnets
  vpc_id          = "vpc-12345678"                           # Add your VPC ID
  node_groups = {
    my_nodes = {
      desired_capacity = 2
      max_capacity     = 2
      min_capacity     = 1
      instance_type    = "t3.medium"
    }
  }
}

# S3 bucket
resource "aws_s3_bucket" "my_bucket" {
  bucket = "my-bucket"
  acl    = "private"
}

# RDS instance
resource "aws_db_instance" "my_db_instance" {
  identifier            = "my-db-instance"
  allocated_storage     = 20
  storage_type          = "gp2"
  engine                = "postgres"
  engine_version        = "12.7"
  instance_class        = "db.t3.micro"
  name                  = "mydatabase"
  username              = "admin"
  password              = "mypassword"
}
