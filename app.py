from flask import Flask, request, jsonify
import os
import subprocess
import sqlite3

app = Flask(__name__)

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DATABASE'] = 'database.db'  # SQLite database file


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def scan_file(filename):
    # Virus scanning logic (assuming ClamAV is used)
    result = subprocess.run(['clamscan', '--stdout', filename], capture_output=True)
    if result.returncode == 0:
        return True, result.stdout.decode('utf-8')
    else:
        return False, result.stderr.decode('utf-8')


def insert_file_record(filename, scan_result, user_id):
    conn = sqlite3.connect(app.config['DATABASE'])
    cursor = conn.cursor()
    cursor.execute("INSERT INTO files (filename, virus_scan_result, user_id) VALUES (?, ?, ?)",
                   (filename, scan_result, user_id))
    conn.commit()
    conn.close()


@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})

    if file and allowed_file(file.filename):
        filename = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(filename)

        # Scan the uploaded file for viruses
        is_safe, scan_result = scan_file(filename)

        if is_safe:
            # Insert record into the database
            insert_file_record(file.filename, scan_result, user_id=1)  # Assuming user ID 1 for simplicity
            return jsonify({'message': 'File uploaded and scanned successfully', 'scan_result': scan_result})
        else:
            # Remove the file if it's infected
            os.remove(filename)
            return jsonify({'error': 'Virus detected in the uploaded file', 'scan_result': scan_result})

    else:
        return jsonify({'error': 'File type not allowed'})


if __name__ == '__main__':
    app.run(debug=True)
